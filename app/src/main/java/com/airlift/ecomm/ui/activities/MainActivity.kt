package com.airlift.ecomm.ui.activities

import android.os.Bundle
import android.support.v4.app.Fragment
import com.airlift.ecomm.R
import com.airlift.ecomm.ui.fragments.ProductListFragment
import com.airlift.ecomm.utils.Common
import dagger.android.AndroidInjection
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.HasSupportFragmentInjector
import javax.inject.Inject



class MainActivity : BaseActivity() , HasSupportFragmentInjector {

    var TAG = MainActivity::class.java.simpleName

    @Inject
    lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Fragment>

    override fun supportFragmentInjector(): AndroidInjector<Fragment> {
        return dispatchingAndroidInjector
    }

    override fun onCreate(savedInstanceState: Bundle?) {

        //Pass this activity to Android Injector
        AndroidInjection.inject(this)

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //Add fragment to container
        Common.replaceFragment(
                supportFragmentManager,
                ProductListFragment.newInstance(),
                R.id.main_content, false)

    }
}
