package com.airlift.ecomm.ui.activities


import android.os.Bundle
import android.support.v4.content.ContextCompat
import com.airlift.ecomm.R
import com.airlift.ecomm.ui.items.ProductItem
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.activity_detail.*
import kotlinx.android.synthetic.main.content_detail.*

class DetailsActivity : BaseActivity() {

    val TAG: String = DetailsActivity::class.java.simpleName

    companion object {

        var EXTRA_ITEM = "data"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)

        //Setup toolbar
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowHomeEnabled(true)

        //Get details of PostItem
        if (intent.extras.get(EXTRA_ITEM) != null) {
            val postItem = intent.extras.get(EXTRA_ITEM) as ProductItem
            setContent(postItem)
        }
    }

    private fun setContent(productItem: ProductItem) {

        //Set content in Collapsing Toolbar
        toolbar_layout.title = productItem.title
        toolbar_layout.setCollapsedTitleTextAppearance(R.style.collapsedAppBar)
        toolbar_layout.setExpandedTitleTextAppearance(R.style.expandedAppBar)
        toolbar_layout.setContentScrimColor(ContextCompat.getColor(this, R.color.colorPrimaryDark))
        toolbar_layout.setStatusBarScrimColor(ContextCompat.getColor(this, R.color.colorPrimaryDark))

        Glide.with(this).load(productItem.image).into(img_header)

        //Set content in details section
        txt_title.text = productItem.title
        txt_description.text = productItem.details
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}