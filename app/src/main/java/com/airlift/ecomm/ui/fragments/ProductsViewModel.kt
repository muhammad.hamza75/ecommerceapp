package com.airlift.ecomm.ui.fragments

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.ViewModel
import com.airlift.ecomm.data.entity.Products
import com.airlift.ecomm.data.local.posts.ProductsDataSource
import com.airlift.ecomm.data.network.NetDataSource
import com.airlift.ecomm.ui.items.ProductItem
import com.mikepenz.fastadapter.commons.adapters.FastItemAdapter
import javax.inject.Inject

/**
 * Created by umair on 09/01/2018.
 */
class ProductsViewModel @Inject
internal constructor(private val netDataSource: NetDataSource, var productsDataSource: ProductsDataSource) : ViewModel() {

    fun setupAdapter(): FastItemAdapter<ProductItem> {
        return FastItemAdapter<ProductItem>()
    }

    //Convert post object received from data source to item for RecyclerView adapter
    fun transformToPostItem(product: Products): ProductItem {
        val postItem = ProductItem()
        postItem.title = product.title
        postItem.details = product.price.toString()
        postItem.image = product.image
        return postItem
    }

    //Loads posts from server
    fun loadPosts(): LiveData<List<Products>> {
        return netDataSource.loadPosts()
    }

    //Loads posts from local database
    fun loadSavedPosts(): LiveData<List<Products>> {
        return productsDataSource.getAllPosts()
    }

    //Add post to local database
    fun savePost(post: Products) {
        productsDataSource.insertPost(post)
    }
}