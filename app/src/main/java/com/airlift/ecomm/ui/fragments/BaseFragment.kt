package com.airlift.ecomm.ui.fragments

import android.content.Context
import android.support.v4.app.Fragment
import com.airlift.ecomm.utils.TransitionInformation
import dagger.android.support.AndroidSupportInjection


open class BaseFragment : Fragment() {

    internal val mTransitionInformation = TransitionInformation()

    override fun onAttach(context: Context) {
        super.onAttach(context)
        AndroidSupportInjection.inject(this)
    }
}