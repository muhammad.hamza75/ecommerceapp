package com.airlift.ecomm.di

import com.airlift.ecomm.data.local.db.AppDatabase
import com.airlift.ecomm.data.local.db.dao.ProductsDao
import com.airlift.ecomm.data.local.posts.ProductsDataSource
import com.airlift.ecomm.data.local.posts.ProductsRepository
import com.airlift.ecomm.data.network.APIs
import com.airlift.ecomm.data.network.NetDataSource
import com.airlift.ecomm.data.network.NetRepository
import dagger.Module
import dagger.Provides
import java.util.concurrent.Executor
import javax.inject.Singleton


@Module
class RepositoryModule{

    @Singleton
    @Provides
    fun provideNetDataSource(apiService: APIs): NetDataSource {
        return NetRepository(apiService)
    }

    @Singleton
    @Provides
    fun providePostsDataSource(executor: Executor, productsDao: ProductsDao): ProductsDataSource {
        return ProductsRepository(executor, productsDao)
    }

    @Singleton
    @Provides
    fun providePostsDao(db: AppDatabase): ProductsDao {
        return db.postsDao()
    }
}