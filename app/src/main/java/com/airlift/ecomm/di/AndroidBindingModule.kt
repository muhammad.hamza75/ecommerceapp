package com.airlift.ecomm.di

import com.airlift.ecomm.ui.activities.MainActivity
import com.airlift.ecomm.ui.fragments.ProductListFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector


/**
 * Created by umair on 09/01/2018.
 */
@Module
internal abstract class AndroidBindingModule {

    @ActivityScope
    @ContributesAndroidInjector
    abstract fun mainActivity(): MainActivity

    @FragmentScope
    @ContributesAndroidInjector
    abstract fun postListFragment(): ProductListFragment

}