package com.airlift.ecomm.data.network

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.Transformations
import com.airlift.ecomm.data.entity.Products


class NetRepository(val apiService: APIs) : NetDataSource {

    override fun loadPosts(): LiveData<List<Products>> {

        return Transformations.map(apiService.getProducts(), { response -> response.body })

    }

}