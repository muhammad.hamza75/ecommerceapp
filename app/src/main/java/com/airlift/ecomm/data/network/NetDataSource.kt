package com.airlift.ecomm.data.network

import android.arch.lifecycle.LiveData
import com.airlift.ecomm.data.entity.Products


interface NetDataSource {
    fun loadPosts(): LiveData<List<Products>>
}
