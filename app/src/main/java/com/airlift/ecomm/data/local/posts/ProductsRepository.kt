package com.airlift.ecomm.data.local.posts

import android.arch.lifecycle.LiveData
import com.airlift.ecomm.data.entity.Products
import com.airlift.ecomm.data.local.db.dao.ProductsDao
import java.util.concurrent.Executor
import javax.inject.Inject


class ProductsRepository @Inject constructor(var executor: Executor, var productsDao: ProductsDao) : ProductsDataSource {

    override fun insertPost(products: Products) {
        executor.execute {
            productsDao.insertPost(products)
        }
    }

    override fun getAllPosts(): LiveData<List<Products>> {
        return productsDao.getAllPosts()
    }
}