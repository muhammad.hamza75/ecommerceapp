package com.airlift.ecomm.data.local.posts

import android.arch.lifecycle.LiveData
import com.airlift.ecomm.data.entity.Products


interface ProductsDataSource {

    fun insertPost(products: Products)

    fun getAllPosts(): LiveData<List<Products>>
}