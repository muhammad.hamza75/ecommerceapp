package com.airlift.ecomm.data.local.db

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import com.airlift.ecomm.data.entity.Products
import com.airlift.ecomm.data.local.db.dao.ProductsDao



@Database(entities = arrayOf(Products::class), version = 5)
abstract class AppDatabase : RoomDatabase() {

    abstract fun postsDao(): ProductsDao
}