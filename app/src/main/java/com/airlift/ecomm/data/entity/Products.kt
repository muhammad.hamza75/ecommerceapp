package com.airlift.ecomm.data.entity

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey



@Entity(tableName = "products")
data class Products(

        @PrimaryKey
        @ColumnInfo(name = "id")
        val id: Int = 0,

        @ColumnInfo(name = "title")
        val title: String,

        @ColumnInfo(name = "description")
        val details: String?,

        @ColumnInfo(name = "category")
        val category: String,

        @ColumnInfo(name = "image")
        val image: String,

        @ColumnInfo(name = "price")
        val price: String

)