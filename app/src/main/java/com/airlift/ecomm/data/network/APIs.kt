package com.airlift.ecomm.data.network

import android.arch.lifecycle.LiveData
import com.airlift.ecomm.data.entity.Products
import retrofit2.http.GET

/**
 * Created by umair on 08/01/2018.
 */
interface APIs {

    @GET("products")
    fun getProducts(): LiveData<ApiResponse<List<Products>>>

}