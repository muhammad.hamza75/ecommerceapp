package com.airlift.ecomm.data.local.db.dao

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.*
import com.airlift.ecomm.data.entity.Products



@Dao
interface ProductsDao {

    /**
     * Get a posts by id
     *
     * @param id The id of teh posts
     * @return posts from the table with the specific id
     * */
    @Query("SELECT * from products WHERE id = :id")
    fun getUserById(id: String): LiveData<Products>

    /**
     * Insert a post to the database, if the post already exist, replace it
     *
     * @param products The post to be inserted
     * */
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertPost(products: Products)


    /**
     * Delete all the posts from the database
     * */
    @Query("DELETE FROM products")
    fun deleteAllPosts()

    /**
     * Delete a single Posts from the Database
     *
     * @param products The Posts to be deleted
     * */
    @Delete
    fun deletePost(products: Products)

    /**
     * Deletes a variable number of posts
     * */
    @Delete
    fun deletePosts(vararg posts: Products)

    /**
     * Loads all the posts from the db
     * */
    @Query("SELECT * FROM products")
    fun getAllPosts(): LiveData<List<Products>>
}