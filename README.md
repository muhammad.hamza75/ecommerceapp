# E-commerce App
### Kotlin + Dagger 2 + Retrofit 2 + Room DAO + RxBus + LiveData + ViewModel
##### `A simple android app with Android new Architecture implememtaion`. 

A simple app to display list of Products in RecyclerView using android new architecture implementation. 

![Image1](pictures/image1.png)
![Image2](pictures/image2.png)

* Written in Kotlin
* API calls with Retrofit2
* Room Persistence Library used for Database
* Dagger2 used for dependency injection with **dagger.android** approach
* [FastAdapter](https://github.com/mikepenz/FastAdapter) used for RecyclerView Adapter
* RxBus is used for event emit & listeners
* Android ViewModel is used
                

